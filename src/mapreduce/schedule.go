package mapreduce

import (
	"fmt"
	"sync"
)

//
// schedule() starts and waits for all tasks in the given phase (mapPhase
// or reducePhase). the mapFiles argument holds the names of the files that
// are the inputs to the map phase, one per map task. nReduce is the
// number of reduce tasks. the registerChan argument yields a stream
// of registered workers; each item is the worker's RPC address,
// suitable for passing to call(). registerChan will yield all
// existing registered workers (if any) and new ones as they register.
//

func schedule(jobName string, mapFiles []string, nReduce int, phase jobPhase, registerChan chan string) {
	var ntasks int
	var n_other int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mapFiles)
		n_other = nReduce
	case reducePhase:
		ntasks = nReduce
		n_other = len(mapFiles)
	}

	executors := newExecutors()
	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, n_other)

	// watch the workers
	// only one go thread can modify the workers

	// All ntasks tasks have to be scheduled on workers. Once all tasks
	// have completed successfully, schedule() should return.
	//
	// Your code here (Part III, Part IV).

	// watch the workers change
	go func() {
		for {
			newWorker := <-registerChan
			executors.registerWorker(newWorker)
		}
	}()

	wg := &sync.WaitGroup{}
	for i := 0; i < ntasks; i++ {
		mapId := i
		wg.Add(1)
		executors.submit(func(idleWorker string) bool{
			result := call(idleWorker, "Worker.DoTask", DoTaskArgs{
				JobName: jobName,
				File: mapFiles[mapId],
				Phase: phase,
				TaskNumber: mapId,
				NumOtherPhase:n_other,
			}, nil)
			if result {
				wg.Done()
			}
			return result
		})
	}
	wg.Wait()


	fmt.Printf("Schedule: %v done\n", phase)
}

type Executors struct {
	lock *sync.Mutex
	cond *sync.Cond // condition to check if has idle work
	workers map[string]bool
}

func newExecutors() *Executors{
	executors := &Executors{}
	executors.lock = &sync.Mutex{}
	executors.cond = sync.NewCond(executors.lock)
	executors.workers = make(map[string]bool)
	return executors
}

func (executors *Executors) registerWorker(worker string) {
	executors.lock.Lock()
	defer executors.lock.Unlock()
	executors.workers[worker] = true
	executors.cond.Broadcast() // tell there has new workers
}

func (executors *Executors) removeWorker(worker string) {
	executors.lock.Lock()
	defer executors.lock.Unlock()
	delete(executors.workers, worker)
	executors.cond.Broadcast() // tell there has removed workers
}

func (executors *Executors) submit(task func(idleWorker string) bool){
	executors.lock.Lock()
	defer executors.lock.Unlock()
	idleWorker := executors.findIdleWorker()
	for ;len(idleWorker) == 0; idleWorker = executors.findIdleWorker(){
		executors.cond.Wait()
	}
	executors.workers[idleWorker] = false
	go func() {
		defer executors.cond.Broadcast()
		defer func() {
			executors.lock.Lock()
			defer executors.lock.Unlock()
			executors.workers[idleWorker] = true // set the worker to be idle
		}()
		ok := task(idleWorker)
		if !ok {
			// worker is failure, remove the worker
			executors.removeWorker(idleWorker)
			// reassign the task
			executors.submit(task)
		}
	}()
}

func (executors *Executors) findIdleWorker() string{
	for worker, idle := range executors.workers {
		if idle {
			return worker
		}
	}
	return ""
}
